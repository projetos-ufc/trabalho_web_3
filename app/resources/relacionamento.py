"""Recurso dos Relacionamentos."""
from flask import current_app
from flask_restful import Resource
from flask_restful.reqparse import RequestParser
from flask_jwt_extended import jwt_required
from ..models import Relacionamentos, RelacionamentosSchema, Topicos
from marshmallow.exceptions import ValidationError


class RelacionamentoBase(Resource):
    """Classe Base dos Recursos."""

    parser = RequestParser()
    parser.add_argument("nome")
    parser.add_argument("topico1_id")
    parser.add_argument("topico2_id")


class RelacionamentoTodos(RelacionamentoBase):
    """Classe para Mostrar Todos Relacionamentos."""

    @jwt_required
    def get(self):
        """Retorna todos os relacionamentos."""
        return {
            "relacionamentos": RelacionamentosSchema(many=True).dump(
                Relacionamentos.query.all()
            )
        }


class Relacionamento(RelacionamentoBase):
    """Classe Para Executar CRUD dos Relacionamentos."""

    @jwt_required
    def get(self, id=None):
        """Read de um relacionamento."""
        if id == None:
            return {"status": "relacionamento invalido!"}, 400
        relacionamento = Relacionamentos.query.get(id)
        return ({"relacionamento": RelacionamentosSchema().dump(relacionamento)}, 200) \
            if relacionamento else ({"status": "relacionamento não encontrado!"}, 400)

    @jwt_required
    def post(self, id=None):
        """Create de um relacionamento."""
        try:
            if id:
                return {"status": "erro não envie o id de um relacionamento!"}, 400
            dados = RelacionamentosSchema().load(self.parser.parse_args())
            top1 = Topicos.query.get(dados['topico1_id'])
            top2 = Topicos.query.get(dados['topico2_id'])
            if top1 and top2:
                rel = Relacionamentos(dados['nome'])
                rel.topico1 = top1
                rel.topico2 = top2
                current_app.db.session.add(rel)
                current_app.db.session.commit()
                return {
                    "status": "cadastrado!",
                    "relacionamento": RelacionamentosSchema().dump(rel)
                }, 201
            return {"status": "Topicos relacionados não existem!"}, 400
        except ValidationError as m_error:
            return m_error.normalized_messages(), 400

    @jwt_required
    def put(self, id=None):
        """Update de um relacionamento."""
        try:
            if id == None:
                return {"status": "relacionamento invalido!"}, 400
            dados = RelacionamentosSchema().load(self.parser.parse_args())
            rel = Relacionamentos.query.get(id)
            top1 = Topicos.query.get(dados['topico1_id'])
            top2 = Topicos.query.get(dados['topico2_id'])
            if top1 and top2:
                rel.nome = dados['nome']
                rel.topico1 = top1
                rel.topico2 = top2
                current_app.db.session.add(rel)
                current_app.db.session.commit()
                return {
                    "status": "atualizado!",
                    "relacionamento": RelacionamentosSchema().dump(rel)
                }, 200
            return {"status": "Topicos relacionados não existem!"}, 400
        except ValidationError as m_error:
            return m_error.normalized_messages(), 400

    @jwt_required
    def delete(self, id=None):
        """Delete de um relacionamento."""
        if id == None:
            return {"status": "relacionamento invalido!"}, 400
        rel = Relacionamentos.query.get(id)
        if rel:
            current_app.db.session.delete(rel)
            current_app.db.session.commit()
            return {
                "status": "deletado!"
            }
        return {"status": "erro relacionamento não encontrado!"}, 400
